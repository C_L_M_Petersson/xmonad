#!/bin/sh

. ~/.xmonad/src/sh/getxmonadsettings.sh

echo $@ | sed -e 's/^\w*\ *//; s/\> \+/\n/g' \
        | dmenu -p "$1:" -m 1 -i             \
            -fn "$font"                      \
            -nf "$barNormalFg"               \
            -nb "$barNormalBg"               \
            -sf "$barSelectedFg"             \
            -sb "$barSelectedBg"
