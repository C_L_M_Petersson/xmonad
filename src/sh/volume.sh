#!/bin/sh

SINK=$(pamixer --list-sinks | grep "alsa_output.pci" | cut -d' ' -f1)
MUTED=$(pamixer  --sink $SINK --get-mute  )
VOLUME=$(pamixer --sink $SINK --get-volume)

[ "$MUTED" = "$1" ] &&                                   \
    (  ([ "$VOLUME" -gt 99 ] && echo -n "1.${VOLUME#?}") \
    || ([ "$VOLUME" -gt  9 ] && echo -n "0.$VOLUME"    ) \
    ||                          echo -n "0.0$VOLUME"     \
    )

exit 0
