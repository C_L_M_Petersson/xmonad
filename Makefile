# {{{ GENERAL SETTINGS

all:
	-rm  $(EXECUTABLE)
	make $(EXECUTABLE)

# }}}

# {{{ HASKELL SETTINGS

EXECUTABLE=xmonad-x86_64-linux

HSBUILD=stack build
HSBUILDOPTS=--copy-bins --local-bin-path .
HSCLEAN=stack clean

# }}}

# {{{ COMPILATION

$(EXECUTABLE):
	$(HSBUILD) $(HSBUILDOPTS)

# }}}

# {{{ ADMINISTRATIVE

%/:
	mkdir -p $@

clean:
	-$(HSCLEAN) clean
	-rm *.errors *.yaml.lock *.cabal

# }}}
